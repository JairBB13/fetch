export interface IMascota {
    nombreDueño: string;
    numeroDueño: string;
    nombreMascota: string;
    alta: string;
    sintomas: string;
}