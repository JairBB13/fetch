import { createRouter, createWebHistory } from 'vue-router'
import RegistrarseViewVue from '@/views/RegistrarseView.vue'
import LoginViewVue from '@/views/LoginView.vue'
import CrudViewVue from '@/views/CrudView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginViewVue
    },
    {
      path: '/registrarse',
      name: 'registrarse',
      component: RegistrarseViewVue
    },
    {
      path: '/crud',
      name: 'crud',
      component: CrudViewVue
    }
  ]
})

export default router
